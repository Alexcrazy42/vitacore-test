﻿using Microsoft.OpenApi.Models;
using Services.Configurations.JobsConfigurations;
using Services.Infrastructure;
using Services.Infrastructure.DependencyInjection;
using Store.Db.Migrations;
using Store.Repositories.Infrastructure.DependencyInjection;
using Web.Contracts.Infrastructure;
using Web.Infrastructure;

namespace Web;

public class Startup
{
    private IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        var postgresDatabaseOptions = new PostgresDatabaseOptions();
        Configuration.GetSection(nameof(PostgresDatabaseOptions)).Bind(postgresDatabaseOptions);

        var redisDatabaseOptions = new RedisDatabaseOptions();
        Configuration.GetSection(nameof(RedisDatabaseOptions)).Bind(redisDatabaseOptions);

        services.AddStoreAndRepositoties(postgresDatabaseOptions, redisDatabaseOptions);

        var auctionLotFillServiceOptions = new AuctionLotFillServiceOptions().GetAuctionLotFillServiceOptions(Configuration);
        var closeAuctionLotServiceOptions = new CloseAuctionLotServiceOptions().GetCloseAuctionLotServiceOptions(Configuration);
        var deleteSpoiledLotServiceOptions = new DeleteSpoiledLotServiceOptions().GetDeleteSpoiledLotServiceOptions(Configuration);
        var notificationMessageServiceOptions = new NotificationMessageServiceOptions().GetNotificationMessageServiceOptions(Configuration);
        services.AddServices(Configuration, auctionLotFillServiceOptions, closeAuctionLotServiceOptions, deleteSpoiledLotServiceOptions, notificationMessageServiceOptions);
        services.AddControllers();
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "AuctionService",
                Version = "v1"
            });
        });

        // TODO: заменить BuildServiceProvider
        var options = services.BuildServiceProvider().GetService<JwtOptions>()
            ?? throw new InvalidOperationException("Не заданы JwtOptions");
        services.AddApiAuthentication(options);
        services.AddWebContractsValidation();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseSwagger();
        app.UseSwaggerUI();
        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseCookiePolicy(new CookiePolicyOptions
        {
            MinimumSameSitePolicy = SameSiteMode.Strict,
            HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.Always,
            Secure = CookieSecurePolicy.Always
        });

        app.UseAuthentication();
        app.UseAuthorization();

        
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}