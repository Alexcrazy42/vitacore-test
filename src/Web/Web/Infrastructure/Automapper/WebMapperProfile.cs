﻿using AutoMapper;
using Domain.Models;
using Domain.Models.Enums;
using Services.Abstract.ServiceContracts.Input;
using Services.Abstract.ServiceContracts.Output;
using Web.Contracts.Dtos.Helping;
using Web.Contracts.Dtos.Requests.AuctionLot;
using Web.Contracts.Dtos.Requests.Base;
using Web.Contracts.Dtos.Requests.User;
using Web.Contracts.Dtos.Responses.AuctionLot;
using Web.Contracts.Dtos.Responses.Base;

namespace Web.Infrastructure.Automapper;

public class WebMapperProfile : Profile
{
    public WebMapperProfile()
    {
        CreateMap<AuctionLot, AuctionLotResponse>()
            .ForMember(dest => dest.LotStatusMessage, options => options.MapFrom(src => src.MapLotStatusToRussianString(src.LotStatus)));
          

        CreateMap<AuctionLotStatus, AuctionLotStatusDto>()
            .ConvertUsing((src, dest) =>
            {
                switch (src)
                {
                    case AuctionLotStatus.Active:
                        return AuctionLotStatusDto.Active;
                    case AuctionLotStatus.Sold:
                        return AuctionLotStatusDto.Sold;
                    case AuctionLotStatus.Expired:
                        return AuctionLotStatusDto.Expired;
                    default:
                        throw new InvalidOperationException("Неправильный статус лота!");
                }
            });

        CreateMap<AuctionLotStatusDto, AuctionLotStatus>()
            .ConvertUsing((src, dest) =>
            {
                switch (src)
                {
                    case AuctionLotStatusDto.Active:
                        return AuctionLotStatus.Active;
                    case AuctionLotStatusDto.Sold:
                        return AuctionLotStatus.Sold;
                    case AuctionLotStatusDto.Expired:
                        return AuctionLotStatus.Expired;
                    default:
                        throw new InvalidOperationException("Неправильный статус лота!");
                }
            });

        CreateMap<PagingRequest, PagingServiceRequest>();

        CreateMap(typeof(PagingServiceResponse<>), typeof(BasePagingResponse<>));

        CreateMap<AuctionLotBuyRequest, AuctionLotServiceBuyRequest>();

        CreateMap<AuctionLotMakeBidRequest, AuctionLotServiceMakeBidRequest>();

        CreateMap<UserLoginRequest, LoginUserServiceRequest>();

        CreateMap<UserRegisterRequest, RegisterUserServiceRequest>();
    }
}