﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstract;
using Services.Abstract.ServiceContracts.Input;
using Web.ApiVersioning;
using Web.Contracts.Dtos.Requests.AuctionLot;
using Web.Contracts.Dtos.Requests.Base;
using Web.Contracts.Dtos.Responses.AuctionLot;
using Web.Contracts.Dtos.Responses.Base;
using Web.Contracts.Infrastructure;

namespace Web.Controllers;

[ApiController]
[Authorize]
[Route($"{ApiVersionManager.V1Prefix}/auction-lots")]
public class AuctionLotController : Controller
{
    private IAuctionLotService auctionLotService;
    private IMapper mapper;
    private IValidationService validationService;

    public AuctionLotController(IAuctionLotService auctionLotService, 
        IMapper mapper,
        IValidationService validationService)
    {
        this.mapper = mapper;
        this.auctionLotService = auctionLotService;
        this.validationService = validationService;
    }

    [HttpGet("get-lot")]
    [AllowAnonymous]
    [ProducesResponseType(typeof(BaseResponse<AuctionLotResponse>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<BaseResponse<AuctionLotResponse>> GetAuctionLot(Guid lotId, CancellationToken cancellationToken)
    {
        try
        {
            var auctionLot = await auctionLotService.GetAuctionLot(lotId, cancellationToken);
            var auctionLotResponse = mapper.Map<AuctionLotResponse>(auctionLot);
            return new BaseResponse<AuctionLotResponse>(auctionLotResponse);
        }
        catch(Exception ex)
        {
            Response.StatusCode = StatusCodes.Status404NotFound;
            return new BaseResponse<AuctionLotResponse>(ex.Message);
        }
    }

    [HttpPost("get-lots")]
    [AllowAnonymous]
    [ProducesResponseType(typeof(BaseResponse<List<AuctionLotResponse>>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<BaseResponse<BasePagingResponse<AuctionLotResponse>>> GetLotsOnPage([FromBody] PagingRequest request, CancellationToken cancellationToken)
    {
        try
        {
            await validationService.ValidateAsync(request);
            var pagingServiceContract = mapper.Map<PagingServiceRequest>(request);
            var pagingServiceOutputContract = await auctionLotService.GetLotsPaging(pagingServiceContract, cancellationToken);
            var pagingResponse = mapper.Map<BasePagingResponse<AuctionLotResponse>>(pagingServiceOutputContract);

            Response.StatusCode = StatusCodes.Status200OK;
            return new BaseResponse<BasePagingResponse<AuctionLotResponse>>(pagingResponse);
        }
        catch (Exception ex)
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;
            return new BaseResponse<BasePagingResponse<AuctionLotResponse>>(ex.Message);
        }
    }

    [HttpPut("try-buy-lot")]
    [ProducesResponseType(typeof(BaseResponse<AuctionLotResponse>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<BaseResponse<AuctionLotResponse>> TryBuyLot([FromBody] AuctionLotBuyRequest auctionLotBuyRequest, CancellationToken cancellationToken)
    {
        try
        {
            var serviceInputContract = mapper.Map<AuctionLotServiceBuyRequest>(auctionLotBuyRequest);
            var auctionLot = await auctionLotService.TryBuyLot(serviceInputContract, cancellationToken);
            var auctionLotResponse = mapper.Map<AuctionLotResponse>(auctionLot);
            return new BaseResponse<AuctionLotResponse>(auctionLotResponse);
        }
        catch (Exception ex)
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;
            return new BaseResponse<AuctionLotResponse>(ex.Message);
        }
    }

    [HttpPut("try-make-bid")]
    [ProducesResponseType(typeof(BaseResponse<AuctionLotResponse>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<BaseResponse<AuctionLotResponse>> TryMakeBid([FromBody] AuctionLotMakeBidRequest auctionLotMakeBidRequest, CancellationToken cancellationToken)
    {
        try
        {
            var serviceInputContract = mapper.Map<AuctionLotServiceMakeBidRequest>(auctionLotMakeBidRequest);
            var auctionLot = await auctionLotService.TryMakeBid(serviceInputContract, cancellationToken);
            var auctionLotResponse = mapper.Map<AuctionLotResponse>(auctionLot);
            return new BaseResponse<AuctionLotResponse>(auctionLotResponse);
        }
        catch (Exception ex)
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;
            return new BaseResponse<AuctionLotResponse>(ex.Message);
        }
    }
}