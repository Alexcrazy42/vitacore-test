﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Services.Abstract;
using Services.Abstract.ServiceContracts.Input;
using Web.ApiVersioning;
using Web.Contracts.Dtos.Requests.User;
using Web.Contracts.Infrastructure;
using Web.Infrastructure.Cookies;

namespace Web.Controllers;

[ApiController]
[Route($"{ApiVersionManager.V1Prefix}/users")]
public class UserController : Controller
{
    private readonly IUserService userService;
    private readonly IMapper mapper;
    private readonly IValidationService validationService;

    public UserController(IUserService userService, 
        IMapper mapper, 
        IValidationService validationService)
    {
        this.userService = userService;
        this.mapper = mapper;
        this.validationService = validationService;
    }

    [HttpPost("register")]
    public async Task<IResult> Register([FromBody] UserRegisterRequest request, CancellationToken cancellationToken)
    {
        
        try
        {
            await validationService.ValidateAsync(request);
            var registerUserServiceRequest = mapper.Map<RegisterUserServiceRequest>(request);
            var addedUser = await userService.Register(registerUserServiceRequest, cancellationToken);
            return Results.Ok("Пользователь зарегистрирован");
        }
        catch(Exception ex)
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;
            return Results.BadRequest(ex.Message);
        }
        
    }

    [HttpPost("login")]
    public async Task<IResult> Login([FromBody] UserLoginRequest request, CancellationToken cancellationToken)
    {
        try
        {

            var userLoginServiceRequest = mapper.Map<LoginUserServiceRequest>(request);
            var token = await userService.Login(userLoginServiceRequest, cancellationToken);
            base.Response.Cookies.Append(CookieManager.AuthCookie, token);
            return Results.Ok();
        }
        catch(Exception ex)
        {
            return Results.BadRequest(ex.Message);
        }
        
    }
}