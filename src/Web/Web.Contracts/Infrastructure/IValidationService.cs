﻿namespace Web.Contracts.Infrastructure;

public interface IValidationService
{
    Task ValidateAsync<T>(T value);
}
