﻿using Microsoft.Extensions.DependencyInjection;
using FluentValidation;

namespace Web.Contracts.Infrastructure;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddWebContractsValidation(this IServiceCollection services)
    {
        services.AddTransient<IValidationService, ValidationService>();
        services.AddValidatorsFromAssembly(typeof(ServiceCollectionExtension).Assembly);
        return services;
    }
}
