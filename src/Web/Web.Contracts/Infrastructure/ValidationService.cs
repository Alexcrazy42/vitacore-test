﻿using FluentValidation;

namespace Web.Contracts.Infrastructure;

public class ValidationService : IValidationService
{
    private readonly IServiceProvider serviceProvider;

    public ValidationService(IServiceProvider serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }

    public async Task ValidateAsync<T>(T model)
    {
        var validatorType = typeof(IValidator<>).MakeGenericType(typeof(T));
        var validator = serviceProvider.GetService(validatorType) as IValidator<T>;
        if (validator == null)
        {
            // TODO: дописать unit тест, чтоб это в runtime не падало
            throw new InvalidOperationException($"Validator for type {typeof(T)} not registered");
        }
        var validationResult = await validator.ValidateAsync(model);
        if(!validationResult.IsValid)
        {
            var message = "";
            foreach(var fail in validationResult.Errors)
            {
                message += fail + System.Environment.NewLine;
            }
            throw new ArgumentException(message);
        }

    }
}
