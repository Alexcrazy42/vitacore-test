﻿namespace Web.Contracts.Dtos.Helping;

public enum AuctionLotStatusDto
{
    Sold = 1,
    Expired = 2,
    Active = 4
}
