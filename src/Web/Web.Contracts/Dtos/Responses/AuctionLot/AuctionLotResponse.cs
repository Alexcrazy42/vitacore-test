﻿using Web.Contracts.Dtos.Helping;

namespace Web.Contracts.Dtos.Responses.AuctionLot;

public class AuctionLotResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public decimal CurrentBid { get; set; }

    public Guid? CurrentBidderId { get; set; }

    public decimal WinningBid { get; set; }

    public AuctionLotStatusDto LotStatus { get; set; }

    public string LotStatusMessage { get; set; }

    public DateTime LotExpiredAt { get; set; }

    public DateTime? LotSpoiledAt { get; set; }
}