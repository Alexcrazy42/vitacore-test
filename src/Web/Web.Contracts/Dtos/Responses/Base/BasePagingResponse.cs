﻿namespace Web.Contracts.Dtos.Responses.Base;

public class BasePagingResponse<T>
{
    public int PageCount { get; set; }

    public int TotalCount { get; set; }

    public IReadOnlyCollection<T> PageItems { get; set; } = new List<T>();

    public BasePagingResponse(int pageCount, int totalCount, IReadOnlyCollection<T> pageItems)
    {
        PageCount = pageCount;
        TotalCount = totalCount;
        PageItems = pageItems;
    }
}