﻿namespace Web.Contracts.Dtos.Responses.Base;

public class BaseResponse<T>
{
    public string Message { get; }

    public T Data { get; }

    public BaseResponse(string message, T data)
    {
        Message = message;
        Data = data;
    }

    public BaseResponse(string message)
    {
        Message = message;
    }

    public BaseResponse(T data)
    {
        Data = data;
    }

}