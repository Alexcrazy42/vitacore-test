﻿namespace Web.Contracts.Dtos.Requests.AuctionLot;

public class AuctionLotMakeBidRequest
{
    public Guid UserId { get; set; }

    public Guid AuctionLotId { get; set; }

    public decimal AuctionBid { get; set; }
}