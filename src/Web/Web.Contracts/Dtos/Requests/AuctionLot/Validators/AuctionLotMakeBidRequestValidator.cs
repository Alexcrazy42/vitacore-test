﻿using FluentValidation;

namespace Web.Contracts.Dtos.Requests.AuctionLot.Validators;

public class AuctionLotMakeBidRequestValidator : AbstractValidator<AuctionLotMakeBidRequest>
{
	public AuctionLotMakeBidRequestValidator()
	{
        RuleFor(x => x.UserId).NotNull().NotEmpty().WithMessage("Пользователь не задан!");

        RuleFor(x => x.AuctionLotId).NotNull().NotEmpty().WithMessage("Лот не задан!");

        RuleFor(x => x.AuctionBid).GreaterThan(0).WithMessage("Ставка должна быть больше 0!");
    }
}