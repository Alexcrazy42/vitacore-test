﻿using FluentValidation;

namespace Web.Contracts.Dtos.Requests.AuctionLot.Validators;

public class AuctionLotBuyRequestValidator : AbstractValidator<AuctionLotBuyRequest>
{
	public AuctionLotBuyRequestValidator()
	{
		RuleFor(x => x.UserId).NotNull().NotEmpty().WithMessage("Пользователь не задан!");

		RuleFor(x => x.AuctionLotId).NotNull().NotEmpty().WithMessage("Лот не задан!");
    }
}
