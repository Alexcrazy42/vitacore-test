﻿namespace Web.Contracts.Dtos.Requests.AuctionLot;

public class AuctionLotBuyRequest
{
    public Guid UserId { get; set; }

    public Guid AuctionLotId { get; set; }

    public AuctionLotBuyRequest(Guid userId, Guid auctionLotId)
    {
        UserId = userId;
        AuctionLotId = auctionLotId;
    }
}