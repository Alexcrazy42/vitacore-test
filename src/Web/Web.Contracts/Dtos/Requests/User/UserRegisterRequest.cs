﻿namespace Web.Contracts.Dtos.Requests.User;

public class UserRegisterRequest
{
    public string Email { get; set; }

    public string Password { get; set; }

    public UserRegisterRequest(string email, string password)
    {
        Email = email;
        Password = password;
    }
}