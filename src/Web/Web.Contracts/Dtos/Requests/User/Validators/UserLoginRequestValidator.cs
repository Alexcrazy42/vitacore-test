﻿using FluentValidation;

namespace Web.Contracts.Dtos.Requests.User.Validators;

public class UserLoginRequestValidator : AbstractValidator<UserLoginRequest>
{
    public UserLoginRequestValidator()
    {
        RuleFor(x => x.Email).NotEmpty().NotNull().WithMessage("Email не может быть пустым");

        RuleFor(x => x.Password).NotEmpty().NotNull().WithMessage("Паспорт не может быть пустым");
    }
}
