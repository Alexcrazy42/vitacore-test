﻿using FluentValidation;

namespace Web.Contracts.Dtos.Requests.User.Validators;

public class UserRegisterRequestValidator : AbstractValidator<UserRegisterRequest>
{
    public UserRegisterRequestValidator()
    {
        RuleFor(x => x.Email).EmailAddress().WithName("Email").WithMessage("Неправильно задан email!");

        RuleFor(x => x.Email).NotEmpty().NotNull().WithMessage("Email не может быть пустым!");

        RuleFor(x => x.Password).MinimumLength(10).WithMessage("Длина пароля должна быть не менее 10!");

        RuleFor(x => x.Password).NotNull().NotEmpty().WithMessage("Пароль не может быть пустым!");

        RuleFor(x => x.Password).Must(IsPaswordNotEasy).WithMessage("Пароль слишком простой!");
    }

    private bool IsPaswordNotEasy(string password)
    {
        // логика по котором понимается, простой ли пароль
        return true;
    }
}
