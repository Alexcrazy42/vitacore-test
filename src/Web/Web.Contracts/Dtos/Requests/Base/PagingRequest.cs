﻿namespace Web.Contracts.Dtos.Requests.Base;

public class PagingRequest
{
    public int PageNumber { get; set; }

    public int PageSize { get; set; }

    public PagingRequest(int pageNumber, int pageSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
    }
}