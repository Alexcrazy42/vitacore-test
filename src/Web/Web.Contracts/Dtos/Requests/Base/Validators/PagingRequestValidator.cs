﻿using FluentValidation;

namespace Web.Contracts.Dtos.Requests.Base.Validators;

public class PagingRequestValidator : AbstractValidator<PagingRequest>
{
    public PagingRequestValidator()
    {
        RuleFor(x => x.PageNumber).GreaterThan(0).WithMessage("Номер страницы должен быть больше нуля");
    }
}