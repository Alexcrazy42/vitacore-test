﻿namespace Domain.Models;

public class NotificationMessage
{
    public Guid Id { get; set; }

    public string EmailAdress { get; set; }

    public string Message { get; set; }
}