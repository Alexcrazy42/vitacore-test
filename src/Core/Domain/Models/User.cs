﻿namespace Domain.Models;

public class User
{
    public Guid Id { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }

    public IReadOnlyCollection<AuctionLot> CurrentUserLots { get; set; } = new List<AuctionLot>();

    public IReadOnlyCollection<AuctionLot> WinningUserLots { get; set; } = new List<AuctionLot>();
}
