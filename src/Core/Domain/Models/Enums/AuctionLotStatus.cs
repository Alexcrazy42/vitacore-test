﻿namespace Domain.Models.Enums;

public enum AuctionLotStatus
{
    Sold = 1,
    Expired = 2,
    Active = 4
}