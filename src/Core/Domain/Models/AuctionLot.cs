﻿using Domain.Models.Enums;

namespace Domain.Models;

public class AuctionLot
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public decimal CurrentBid { get; set; }

    public Guid? CurrentBidderId { get; set; }

    public User? CurrentBidder { get; set; }

    public decimal WinningBid { get; set; }

    public Guid? WinningBidderId { get; set; }

    public User? WinningBidder { get; set; }

    public AuctionLotStatus LotStatus { get; set; }

    /// <summary>
    /// Это время, до которого лот еще можно продавать,
    /// после лот уходит последнему ставившему
    /// </summary>
    public DateTime LotExpiredAt { get; set; }

    /// <summary>
    /// Если лот может испортиться (как в случае с мандарином), 
    /// у него есть время до которого, его можно продать
    /// </summary>
    public DateTime? LotSpoiledAt { get; set; }

    public string MapLotStatusToRussianString(AuctionLotStatus lotStatus)
    {
        switch (lotStatus)
        {
            case AuctionLotStatus.Active:
                return "Активен";
            case AuctionLotStatus.Sold:
                return "Продан";
            case AuctionLotStatus.Expired:
                return "Просрочен";
            default:
                throw new InvalidOperationException("Неправильный статус лота!");
        }
    }
}