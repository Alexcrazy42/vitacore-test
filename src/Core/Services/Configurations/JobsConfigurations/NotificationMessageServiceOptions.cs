﻿using Microsoft.Extensions.Configuration;

namespace Services.Configurations.JobsConfigurations;

public class NotificationMessageServiceOptions
{
    public int ExecutionIntervalInSeconds { get; set; }

    public string VisibleName { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }

    public string SmtpServer { get; set; }

    public int SmtpPort { get; set; }

    public NotificationMessageServiceOptions() { }

    public NotificationMessageServiceOptions(int executionIntervalInSeconds, string visibleName, string email, string password, string smtpServer, int smtpPort)
    {
        ExecutionIntervalInSeconds = executionIntervalInSeconds;
        VisibleName = visibleName;
        Email = email;
        Password = password;
        SmtpServer = smtpServer;
        SmtpPort = smtpPort;
    }

    public NotificationMessageServiceOptions GetNotificationMessageServiceOptions(IConfiguration configuration)
    {
        int executionIntervalInSeconds = configuration.GetValue<int>("NotificationMessageServiceOptions:ExecutionIntervalInSeconds");

        string visibleName = configuration.GetValue<string>("NotificationMessageServiceOptions:VisibleName")
            ?? throw new ArgumentNullException("Не задан NotificationMessageServiceOptions:VisibleName");

        string email = configuration.GetValue<string>("NotificationMessageServiceOptions:Email") 
            ?? throw new ArgumentNullException("Не задан NotificationMessageServiceOptions:Email");

        string password = configuration.GetValue<string>("NotificationMessageServiceOptions:Password")
            ?? throw new ArgumentNullException("Не задан NotificationMessageServiceOptions:Password");

        string smtpServer = configuration.GetValue<string>("NotificationMessageServiceOptions:SmtpServer")
            ?? throw new ArgumentNullException("Не задан NotificationMessageServiceOptions:SmtpServer");

        int smtpPort = configuration.GetValue<int>("NotificationMessageServiceOptions:SmtpPort");

        if (executionIntervalInSeconds > 0 && smtpPort > 0)
        {
            return new NotificationMessageServiceOptions(executionIntervalInSeconds, visibleName, email, password, smtpServer, smtpPort);
        }
        throw new InvalidOperationException("Неправильно задан параметр NotificationMessageServiceOptions:ExecutionIntervalInSeconds");
    }
}