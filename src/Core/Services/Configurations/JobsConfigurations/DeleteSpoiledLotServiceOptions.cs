﻿using Microsoft.Extensions.Configuration;

namespace Services.Configurations.JobsConfigurations;

public class DeleteSpoiledLotServiceOptions
{
    public string TargetDateTime;

    public DeleteSpoiledLotServiceOptions() { }

    public DeleteSpoiledLotServiceOptions(string targetDateTime)
    {
        TargetDateTime = targetDateTime;
    }

    public DeleteSpoiledLotServiceOptions GetDeleteSpoiledLotServiceOptions(IConfiguration configuration)
    {
        string? targetTime = configuration.GetValue<string>("DeleteSpoiledLotServiceOptions:TargetDateTime");

        if (TimeSpan.TryParseExact(targetTime, "h\\:mm", null, out TimeSpan taskTime))
        {
            return new DeleteSpoiledLotServiceOptions(targetTime);
        }
        else
        {
            throw new InvalidOperationException("Не удалось распарсить время из DeleteSpoiledLotServiceOptions:TargetDateTime");
        }
    }
}
