﻿using Microsoft.Extensions.Configuration;

namespace Services.Configurations.JobsConfigurations;

public class CloseAuctionLotServiceOptions
{
    public int ExecutionIntervalInSeconds;

    public CloseAuctionLotServiceOptions() { }

    public CloseAuctionLotServiceOptions(int executionIntervalInSeconds)
    {
        ExecutionIntervalInSeconds = executionIntervalInSeconds;
    }

    public CloseAuctionLotServiceOptions GetCloseAuctionLotServiceOptions(IConfiguration configuration)
    {
        int executionIntervalInSeconds = configuration.GetValue<int>("CloseAuctionLotServiceOptions:ExecutionIntervalInSeconds");
        if (executionIntervalInSeconds > 0)
        {
            return new CloseAuctionLotServiceOptions(executionIntervalInSeconds);
        }
        throw new InvalidOperationException("Неправильно задан параметр CloseAuctionLotServiceOptions:ExecutionIntervalInSeconds");
    }
}
