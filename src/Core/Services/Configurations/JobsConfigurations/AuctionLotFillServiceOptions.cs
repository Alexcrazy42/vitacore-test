﻿using Microsoft.Extensions.Configuration;

namespace Services.Configurations.JobsConfigurations;

public class AuctionLotFillServiceOptions
{
    public int? ExecutionIntervalInSeconds;
    public string? TargetDateTime;

    public AuctionLotFillServiceOptions() { }

    public AuctionLotFillServiceOptions(int executionIntervalInSeconds)
    {
        ExecutionIntervalInSeconds = executionIntervalInSeconds;
    }

    public AuctionLotFillServiceOptions(string targetDateTime)
    {
        TargetDateTime = targetDateTime;
    }

    public AuctionLotFillServiceOptions GetAuctionLotFillServiceOptions(IConfiguration configuration)
    {
        int executionIntervalInSeconds = configuration.GetValue<int>("AuctionLotFillServiceOptions:ExecutionIntervalInSeconds");
        string? targetTime = configuration.GetValue<string>("AuctionLotFillServiceOptions:TargetDateTime");

        if (executionIntervalInSeconds > 0)
        {
            return new AuctionLotFillServiceOptions(executionIntervalInSeconds);
        }
        else if (targetTime != null)
        {

            if (TimeSpan.TryParseExact(targetTime, "h\\:mm", null, out TimeSpan taskTime))
            {
                return new AuctionLotFillServiceOptions(targetTime);
            }
            else
            {
                throw new InvalidOperationException("Не удалось распарсить время из AuctionLotFillServiceOptions:TargetDateTime");
            }

        }
        else
        {
            throw new InvalidOperationException("Неправильно заданы параметры AuctionLotFillServiceOptions");
        }
    }
}