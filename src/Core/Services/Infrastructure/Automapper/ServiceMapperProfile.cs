﻿using AutoMapper;
using Domain.Models;
using Domain.Models.Enums;
using Services.Abstract.ServiceContracts.Input;
using Services.Abstract.ServiceContracts.Output;
using Store.Db.DbRecords;
using Store.Repositories.Abstract.StoreContracts;
using Store.Repositories.Abstract.StoreContracts.Input;
using Store.Repositories.Abstract.StoreContracts.Output;

namespace Services.Infrastructure.Automapper;

public class ServiceMapperProfile : Profile
{
    public ServiceMapperProfile()
    {
        CreateMap<AuctionLotRecord, AuctionLot>();

        CreateMap<AuctionLot, AuctionLotRecord>();

        CreateMap<UserRecord, User>();

        CreateMap<User, UserRecord>();

        CreateMap<AuctionLotStatusRecord, AuctionLotStatus>()
            .ConvertUsing((src, dest) =>
            {
                switch (src)
                {
                    case AuctionLotStatusRecord.Active:
                        return AuctionLotStatus.Active;
                    case AuctionLotStatusRecord.Sold:
                        return AuctionLotStatus.Sold;
                    case AuctionLotStatusRecord.Expired:
                        return AuctionLotStatus.Expired;
                    default:
                        throw new InvalidOperationException("Неправильный статус лота!");
                }
            });

        CreateMap<AuctionLotStatus, AuctionLotStatusRecord>()
            .ConvertUsing((src, dest) =>
            {
                switch (src)
                {
                    case AuctionLotStatus.Active:
                        return AuctionLotStatusRecord.Active;
                    case AuctionLotStatus.Sold:
                        return AuctionLotStatusRecord.Sold;
                    case AuctionLotStatus.Expired:
                        return AuctionLotStatusRecord.Expired;
                    default:
                        throw new InvalidOperationException("Неправильный статус лота!");
                }
            });

        CreateMap<PagingServiceRequest, PagingStoreRequest>();

        CreateMap(typeof(PagingStoreResponse<>), typeof(PagingServiceResponse<>));

        CreateMap<AuctionLotServiceBuyRequest, AuctionLotStoreBuyRequest>();

        CreateMap<AuctionLotServiceMakeBidRequest, AuctionLotStoreMakeBidRequest>();

        CreateMap<NotificationMessage, NotificationMessageRecord>();

        CreateMap<NotificationMessageRecord, NotificationMessage>();

        CreateMap<RegisterUserServiceRequest, RegisterUserStoreRequest>();
    }
}