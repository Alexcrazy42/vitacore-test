﻿using Microsoft.Extensions.Configuration;
using Services.Configurations.JobsConfigurations;

namespace Services.Infrastructure;

public class JwtOptions
{
    public string SecretKey { get; set; }

    public int ExpiresHours { get; set; }
     
    public JwtOptions() { }

    public JwtOptions(string secretKey, int expiresHours)
    {
        SecretKey = secretKey;
        ExpiresHours = expiresHours;
    }

    public JwtOptions GetJwtOptions(IConfiguration configuration)
    {
        string secretKey = configuration.GetValue<string>("JwtOptions:SecretKey")
            ?? throw new InvalidOperationException("Не задан JwtOptions:SecretKey");
        int expiresHours = configuration.GetValue<int>("JwtOptions:ExpiresHours");

        if (expiresHours > 0)
        {
            return new JwtOptions(secretKey, expiresHours);
        }
        else
        {
            throw new InvalidOperationException("Неправильно заданы параметры JwtOptions");
        }
    }
}
