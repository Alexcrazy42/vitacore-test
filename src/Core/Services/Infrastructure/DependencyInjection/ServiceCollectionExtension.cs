﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Abstract;
using Services.Configurations.JobsConfigurations;
using Services.Implementations;
using Services.Implementations.JobServices;
using Store.Repositories.Abstract;

namespace Services.Infrastructure.DependencyInjection;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddServices(this IServiceCollection services,
        IConfiguration configuration,
        AuctionLotFillServiceOptions auctionLotFillServiceOptions, 
        CloseAuctionLotServiceOptions closeAuctionLotServiceOptions, 
        DeleteSpoiledLotServiceOptions deleteSpoiledLotServiceOptions, 
        NotificationMessageServiceOptions notificationMessageServiceOptions)
    {
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        services.AddSingleton(auctionLotFillServiceOptions);
        services.AddSingleton(closeAuctionLotServiceOptions);
        services.AddSingleton(deleteSpoiledLotServiceOptions);
        services.AddSingleton(notificationMessageServiceOptions);
        services.AddHostedService<AuctionLotFillService>();
        services.AddHostedService<CloseAuctionLotService>();
        services.AddHostedService<DeleteSpoiledLotService>();
        services.AddHostedService<NotificationMessageService>();


        services.AddScoped<IAuctionLotService, AuctionLotService>();
        services.AddScoped<IJwtProvider, JwtProvider>();
        services.AddScoped<IUserService, UserService>();

        JwtOptions jwtOptions = new JwtOptions().GetJwtOptions(configuration);
        services.AddSingleton<JwtOptions>(jwtOptions);

        return services;
    }
}