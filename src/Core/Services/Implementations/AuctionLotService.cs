﻿using AutoMapper;
using Domain.Models;
using Services.Abstract;
using Services.Abstract.ServiceContracts.Input;
using Services.Abstract.ServiceContracts.Output;
using Store.Repositories.Abstract;
using Store.Repositories.Abstract.StoreContracts.Input;

namespace Services.Implementations;

public class AuctionLotService : IAuctionLotService
{
    private IAuctionLotRepository auctionLotRepository;
    private IMapper mapper;

    public AuctionLotService(IAuctionLotRepository auctionLotRepository, IMapper mapper)
    {
        this.auctionLotRepository = auctionLotRepository;
        this.mapper = mapper;
    }

    public async Task<AuctionLot> GetAuctionLot(Guid id, CancellationToken cancellationToken)
    {
        var auctionLotRecord = await auctionLotRepository.GetAuctionLotRecord(id, cancellationToken);
        return mapper.Map<AuctionLot>(auctionLotRecord);
    }

    public async Task<PagingServiceResponse<AuctionLot>> GetLotsPaging(PagingServiceRequest contract, CancellationToken cancellationToken)
    {
        var pagingStoreContract = mapper.Map<PagingStoreRequest>(contract);
        var pagingStoreOutputContract = await auctionLotRepository.GetPagingLots(pagingStoreContract, cancellationToken);
        return mapper.Map<PagingServiceResponse<AuctionLot>>(pagingStoreOutputContract);
    }

    public async Task<AuctionLot> TryBuyLot(AuctionLotServiceBuyRequest inputContract, CancellationToken cancellationToken)
    {
        var storeInputContract = mapper.Map<AuctionLotStoreBuyRequest>(inputContract);
        var auctionLotRecord = await auctionLotRepository.TryBuyLot(storeInputContract, cancellationToken);
        return mapper.Map<AuctionLot>(auctionLotRecord);
    }

    public async Task<AuctionLot> TryMakeBid(AuctionLotServiceMakeBidRequest inputContract, CancellationToken cancellationToken)
    {
        var storeInputContract = mapper.Map<AuctionLotStoreMakeBidRequest>(inputContract);
        var auctionLotRecord = await auctionLotRepository.TryMakeBid(storeInputContract, cancellationToken);
        return mapper.Map<AuctionLot>(auctionLotRecord);
    }
}