﻿using AutoMapper;
using Domain.Models;
using Services.Abstract;
using Services.Abstract.ServiceContracts.Input;
using Store.Repositories.Abstract;
using Store.Repositories.Abstract.StoreContracts.Input;

namespace Services.Implementations;

public class UserService : IUserService
{
    private IUserRepository userRepository;
    private IMapper mapper;
    private readonly IPasswordHasher passwordHasher;
    private readonly IJwtProvider jwtProvider;

    public UserService(IUserRepository userRepository, 
        IMapper mapper, 
        IPasswordHasher passwordHasher, 
        IJwtProvider jwtProvider)
    {
        this.userRepository = userRepository;
        this.mapper = mapper;
        this.passwordHasher = passwordHasher;
        this.jwtProvider = jwtProvider;
    }

    public async Task<string> Login(LoginUserServiceRequest request, CancellationToken cancellationToken)
    {
        var userRecord = await userRepository.GetUserByEmail(request.Email, cancellationToken);

        var result = passwordHasher.VerifyPassword(request.Password, userRecord.Password);

        if(result == false)
        {
            throw new InvalidOperationException("Неправильный пароль!");
        }

        var user = mapper.Map<User>(userRecord);
        var token = jwtProvider.GenerateJwtToken(user);

        return token;
    }

    public async Task<User> Register(RegisterUserServiceRequest request, CancellationToken cancellationToken)
    {
        var userToAddRecord = mapper.Map<RegisterUserStoreRequest>(request);
        var addedRecord = await userRepository.RegisterUser(userToAddRecord, cancellationToken);
        return mapper.Map<User>(addedRecord);
    }
}