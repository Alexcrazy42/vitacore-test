﻿using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Configurations.JobsConfigurations;
using Store.Repositories.Abstract;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Mail;

namespace Services.Implementations.JobServices;

public class NotificationMessageService : BackgroundService, IDisposable
{
    private NotificationMessageServiceOptions options;
    private readonly IServiceScope _scope;
    private readonly IMapper mapper;

    public NotificationMessageService(NotificationMessageServiceOptions options,
        IServiceProvider serviceProvider, 
        IMapper mapper)
    {
        this.options = options;
        _scope = serviceProvider.CreateScope();
        this.mapper = mapper;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            if (options.ExecutionIntervalInSeconds != 0)
            {
                await Task.Delay(TimeSpan.FromSeconds(options.ExecutionIntervalInSeconds), stoppingToken);
                
                var messages = await GetNotificationMessages(stoppingToken);

                var sendedMessageIds = new List<Guid>();

                var client = new SmtpClient(options.SmtpServer, options.SmtpPort)
                {
                    Credentials = new NetworkCredential(options.Email, options.Password),
                    EnableSsl = true
                };

                foreach (var message in messages)
                {
                    MailAddress from = new MailAddress(options.Email, options.VisibleName);
                    MailAddress to = new MailAddress(message.EmailAdress);
                    
                    MailMessage m = new MailMessage(from, to);
                    m.Body = $"<h2>{message.Message}</h2>";
                    m.IsBodyHtml = true;

                    client.Send(m);

                    sendedMessageIds.Add(message.Id);
                }
                await DeleteSendedMessages(sendedMessageIds, stoppingToken);

                
            }
        }
    }

    private async Task<List<NotificationMessage>> GetNotificationMessages(CancellationToken cancellationToken)
    {
        using (var context = _scope.ServiceProvider.GetRequiredService<IAuctionLotRepository>())
        {
            var notificationMessageRecords = await context.GetNotificationMessageRecords(cancellationToken);
            var notificationMessages = mapper.Map<List<NotificationMessage>>(notificationMessageRecords);
            return notificationMessages;
        }
    }

    private async Task DeleteSendedMessages(IReadOnlyCollection<Guid> messagesIds, CancellationToken cancellationToken)
    {
        using (var context = _scope.ServiceProvider.GetRequiredService<IAuctionLotRepository>())
        {
            await context.DeleteNotificationMessages(messagesIds, cancellationToken);
        }
    }
}