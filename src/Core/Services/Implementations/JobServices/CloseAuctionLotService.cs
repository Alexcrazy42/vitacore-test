﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Configurations.JobsConfigurations;
using Store.Repositories.Abstract;

namespace Services.Implementations.JobServices;

public class CloseAuctionLotService : BackgroundService, IDisposable
{
    private CloseAuctionLotServiceOptions options;
    private readonly IServiceScope _scope;

    public CloseAuctionLotService(CloseAuctionLotServiceOptions options,
        IServiceProvider serviceProvider)
    {
        this.options = options;
        _scope = serviceProvider.CreateScope();
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {

            if (options.ExecutionIntervalInSeconds != 0)
            {
                await Task.Delay(TimeSpan.FromSeconds(options.ExecutionIntervalInSeconds), stoppingToken);
                await CloseAuctionLots(stoppingToken);
            }
        }
    }

    private async Task CloseAuctionLots(CancellationToken cancellationToken)
    {
        using (var context = _scope.ServiceProvider.GetRequiredService<IAuctionLotRepository>())
        {
            await context.CloseAuctionLot(cancellationToken);
        }
    }
}
