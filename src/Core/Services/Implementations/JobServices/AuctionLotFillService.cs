﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Configurations.JobsConfigurations;
using Store.Repositories.Abstract;

namespace Services.Implementations.JobServices;

public class AuctionLotFillService : BackgroundService, IDisposable
{
    private AuctionLotFillServiceOptions options;
    private readonly IServiceScope _scope;

    public AuctionLotFillService(AuctionLotFillServiceOptions options,
        IServiceProvider serviceProvider)
    {
        this.options = options;
        _scope = serviceProvider.CreateScope();
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {

            if (options.ExecutionIntervalInSeconds != 0 && options.ExecutionIntervalInSeconds != null)
            {
                await Task.Delay(TimeSpan.FromSeconds((double)options.ExecutionIntervalInSeconds), stoppingToken);
                await AddNewAuctionLot(stoppingToken);
            }
            else if (options.TargetDateTime != null)
            {
                await MakeDelayBeforeDailyExecution(stoppingToken);
                await AddNewAuctionLot(stoppingToken);
                double timeToDelayInMinutes = 23 * 60 + 59;
                await Task.Delay(TimeSpan.FromMinutes(timeToDelayInMinutes), stoppingToken);
            }
        }
    }

    private async Task AddNewAuctionLot(CancellationToken cancellationToken)
    {
        using (var context = _scope.ServiceProvider.GetRequiredService<IAuctionLotRepository>())
        {
            await context.CreateRandomMandarinLot(cancellationToken);
        }
    }

    private async Task MakeDelayBeforeDailyExecution(CancellationToken cancellationToken)
    {
        int targetHour = int.Parse(options.TargetDateTime.Split(":")[0]);
        int targetMinute = int.Parse(options.TargetDateTime.Split(":")[1]);
        DateTime now = DateTime.Now;
        DateTime targetTime = new DateTime(now.Year, now.Month, now.Day, targetHour, targetMinute, 0);
        if (now > targetTime)
        {
            targetTime.AddDays(1);
        }
        int delay = (int)(targetTime - now).TotalMilliseconds;

        if (delay > 0)
        {
            await Task.Delay(delay, cancellationToken);
        }
    }

    public override void Dispose()
    {
        _scope?.Dispose();
        GC.SuppressFinalize(this);
    }
}
