﻿using Domain.Models;

namespace Services.Abstract;

public interface IJwtProvider
{
    public string GenerateJwtToken(User user);
}
