﻿using Domain.Models;
using Services.Abstract.ServiceContracts.Input;

namespace Services.Abstract;

public interface IUserService
{
    public Task<User> Register(RegisterUserServiceRequest request, CancellationToken cancellationToken);

    public Task<string> Login(LoginUserServiceRequest request, CancellationToken cancellationToken);
}