﻿namespace Services.Abstract.ServiceContracts.Output;

public class PagingServiceResponse<T>
{
    public int PageCount { get; set; }

    public int TotalCount { get; set; }

    public IReadOnlyCollection<T> PageItems { get; set; } = new List<T>();

    public PagingServiceResponse(int pageCount, int totalCount, IReadOnlyCollection<T> pageItems)
    {
        PageCount = pageCount;
        TotalCount = totalCount;
        PageItems = pageItems;
    }
}