﻿namespace Services.Abstract.ServiceContracts.Input;

public class AuctionLotServiceBuyRequest
{
    public Guid UserId { get; set; }

    public Guid AuctionLotId { get; set; }

    public AuctionLotServiceBuyRequest(Guid userId, Guid auctionLotId)
    {
        UserId = userId;
        AuctionLotId = auctionLotId;
    }
}
