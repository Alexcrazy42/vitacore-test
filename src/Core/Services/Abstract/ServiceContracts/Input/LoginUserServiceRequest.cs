﻿namespace Services.Abstract.ServiceContracts.Input;

public class LoginUserServiceRequest
{
    public string Email { get; set; }

    public string Password { get; set; }

    public LoginUserServiceRequest(string email, string password)
    {
        Email = email;
        Password = password;
    }
}
