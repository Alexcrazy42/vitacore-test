﻿namespace Services.Abstract.ServiceContracts.Input;

public class RegisterUserServiceRequest
{
    public string Email { get; set; }

    public string Password { get; set; }

    public RegisterUserServiceRequest(string email, string password)
    {
        Email = email;
        Password = password;
    }
}
