﻿namespace Services.Abstract.ServiceContracts.Input;

public class PagingServiceRequest
{
    public int PageNumber { get; set; }

    public int PageSize { get; set; }

    public PagingServiceRequest(int pageNumber, int pageSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
    }
}
