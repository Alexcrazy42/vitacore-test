﻿namespace Services.Abstract.ServiceContracts.Input;

public class AuctionLotServiceMakeBidRequest
{
    public Guid UserId { get; set; }

    public Guid AuctionLotId { get; set; }

    public decimal AuctionBid { get; set; }
}