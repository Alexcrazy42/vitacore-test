﻿using Domain.Models;
using Services.Abstract.ServiceContracts.Input;
using Services.Abstract.ServiceContracts.Output;

namespace Services.Abstract;

public interface IAuctionLotService
{
    public Task<AuctionLot> GetAuctionLot(Guid id, CancellationToken cancellationToken);

    public Task<PagingServiceResponse<AuctionLot>> GetLotsPaging(PagingServiceRequest contract, CancellationToken cancellationToken);

    public Task<AuctionLot> TryBuyLot(AuctionLotServiceBuyRequest inputContract, CancellationToken cancellationToken);

    public Task<AuctionLot> TryMakeBid(AuctionLotServiceMakeBidRequest inputContract, CancellationToken cancellationToken);
}