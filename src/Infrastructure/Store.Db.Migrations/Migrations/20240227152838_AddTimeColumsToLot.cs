﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Store.Db.Migrations.Migrations
{
    /// <inheritdoc />
    public partial class AddTimeColumsToLot : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LotExpiredAt",
                table: "AuctionLots",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LotSpoiledAt",
                table: "AuctionLots",
                type: "timestamp with time zone",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LotExpiredAt",
                table: "AuctionLots");

            migrationBuilder.DropColumn(
                name: "LotSpoiledAt",
                table: "AuctionLots");
        }
    }
}
