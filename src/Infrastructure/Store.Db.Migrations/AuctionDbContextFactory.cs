﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Store.Db;

namespace Store.Db.Migrations;

public class AuctionDbContextFactory : IDesignTimeDbContextFactory<AuctionDbContext>
{
    private const string JsonFileName = "appsettings.Migrations.json";

    public AuctionDbContext CreateDbContext(string[] args)
    {
        var migrationAssemblyName = typeof(AuctionDbContextFactory).Assembly.GetName().Name;

        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile(JsonFileName)
            .Build();

        var databaseOptions = new PostgresDatabaseOptions();
        configuration.GetSection(nameof(PostgresDatabaseOptions)).Bind(databaseOptions);

        var optionsBuilder = new DbContextOptionsBuilder<AuctionDbContext>();
        optionsBuilder.UseNpgsql(
            connectionString: databaseOptions?.ConnectionString
                              ?? throw new InvalidOperationException($"DB connection string is not specified!"),
            x => x.MigrationsAssembly(migrationAssemblyName));

        return new AuctionDbContext(optionsBuilder.Options);
    }
}