﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Store.Db;
using Store.Db.Configurations;
using Store.Db.Migrations;
using Store.Repositories.Abstract;
using Store.Repositories.Implementations;

namespace Store.Repositories.Infrastructure.DependencyInjection;

public static class ServiceCollectionExtension
{

    public static IServiceCollection AddStoreAndRepositoties(this IServiceCollection services,
        DatabaseOptions optionsSettings, DatabaseOptions cacheOptionsSettings)
    {
        services.AddDbContext<AuctionDbContext>(options => options.ConfigureDbOptions(optionsSettings));
        services.AddScoped<IAuctionLotRepository, AuctionLotRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IPasswordHasher, PasswordHasher>();
        return services;
    }

    private static void ConfigureDbOptions(this DbContextOptionsBuilder options, DatabaseOptions settings)
    {
        switch (settings.Provider)
        {
            case DbProvider.PostgreSQL:
                options.UseNpgsql(
                    settings.ConnectionString,
                    x => x.MigrationsAssembly(typeof(AuctionDbContextFactory).Assembly.GetName().Name));
                break;

            default:
                throw new ArgumentException($"DB provider is incorrect.");
        }

        options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
    }
}