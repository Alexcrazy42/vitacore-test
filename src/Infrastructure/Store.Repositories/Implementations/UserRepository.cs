﻿using Microsoft.EntityFrameworkCore;
using Store.Db;
using Store.Db.DbRecords;
using Store.Repositories.Abstract;
using Store.Repositories.Abstract.StoreContracts.Input;

namespace Store.Repositories.Implementations;

public class UserRepository : IUserRepository
{
    private readonly AuctionDbContext auctionDbContext;
    private readonly IPasswordHasher passwordHasher;

    public UserRepository(AuctionDbContext auctionDBContext, 
        IPasswordHasher passwordHasher)
    {
        this.auctionDbContext = auctionDBContext;
        this.passwordHasher = passwordHasher;
    }

    public async Task<UserRecord> RegisterUser(RegisterUserStoreRequest request, CancellationToken cancellationToken)
    {
        var existedUserRecord = await auctionDbContext.Users.FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken);

        if (existedUserRecord != null) 
        {
            throw new InvalidOperationException("Пользователь с таким email уже существует!");
        }

        var userRecordToAdd = new UserRecord()
        {
            Id = Guid.NewGuid(),
            Email = request.Email,
            Password = passwordHasher.GenerateHashPassword(request.Password)
        };

        await auctionDbContext.Users.AddAsync(userRecordToAdd);
        await auctionDbContext.SaveChangesAsync(cancellationToken);
        return userRecordToAdd;
    }

    public async Task<UserRecord> GetUserByEmail(string email, CancellationToken cancellationToken)
    {
        var userRecord = await auctionDbContext.Users
            .FirstOrDefaultAsync(x => x.Email == email, cancellationToken);

        if (userRecord == null)
        {
            throw new InvalidOperationException("Пользователь не найден!");
        }

        return userRecord;
    }
}