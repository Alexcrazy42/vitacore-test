﻿using Store.Repositories.Abstract;

namespace Store.Repositories.Implementations;

public class PasswordHasher : IPasswordHasher
{
    public string GenerateHashPassword(string password)
    {
        return BCrypt.Net.BCrypt.EnhancedHashPassword(password);
    }

    public bool VerifyPassword(string password, string hashedPassword)
    {
        return BCrypt.Net.BCrypt.EnhancedVerify(password, hashedPassword);
    }
}
