﻿using Microsoft.EntityFrameworkCore;
using Store.Db;
using Store.Db.DbRecords;
using Store.Repositories.Abstract;
using Store.Repositories.Abstract.StoreContracts.Input;
using Store.Repositories.Abstract.StoreContracts.Output;

namespace Store.Repositories.Implementations;

public class AuctionLotRepository : IAuctionLotRepository
{
    private AuctionDbContext auctionDbContext;

    public AuctionLotRepository(AuctionDbContext auctionDbContext)
    {
        this.auctionDbContext = auctionDbContext;
    }

    public async Task CreateRandomMandarinLot(CancellationToken cancellationToken)
    {
        // если представить реальную ситуацию создания лота на аукционе:
        // цену, время жизни лота, и срок действия продукта (если это продукт) указывал бы размещающий этот лот
        var generatedMandarinLot = new AuctionLotRecord
        {
            Id = Guid.NewGuid(),
            Name = "Мандарин",
            CurrentBid = 0,
            WinningBid = new Random().Next(0, 1000),
            LotStatus = AuctionLotStatusRecord.Active,
            LotExpiredAt = DateTime.UtcNow.AddSeconds(30),
            LotSpoiledAt = DateTime.UtcNow.AddDays(1)
        };

        await auctionDbContext.AddAsync(generatedMandarinLot);
        await auctionDbContext.SaveChangesAsync();

        Console.WriteLine($"Created mandarin lot with id = {generatedMandarinLot.Id} at {DateTime.UtcNow}");
    }

    public async Task<AuctionLotRecord> GetAuctionLotRecord(Guid id, CancellationToken cancellationToken)
    {
        var auctionLotRecord = await auctionDbContext.AuctionLots
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (auctionLotRecord != null)
        {
            return auctionLotRecord;
        }
        throw new InvalidOperationException("Такого лота не найдено!");
    }
    public async Task CloseAuctionLot(CancellationToken cancellationToken)
    {
        using(var transaction = auctionDbContext.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        {
            try
            {
                var lotsToClose = await auctionDbContext.AuctionLots
                    .Where(x => x.LotExpiredAt < DateTime.UtcNow)
                    .ToListAsync();

                foreach (var lot in lotsToClose)
                {
                    if (lot.CurrentBidderId != null)
                    {
                        lot.WinningBidder = await auctionDbContext.Users
                            .FirstOrDefaultAsync(x => x.Id == lot.CurrentBidderId, cancellationToken);
                        var notificationMessage = new NotificationMessageRecord()
                        {
                            Id = Guid.NewGuid(),
                            EmailAdress = lot.WinningBidder.Email,
                            Message = $"Чек по лоту {lot.Id}"
                        };

                        await auctionDbContext.NotificationMessages
                            .AddAsync(notificationMessage, cancellationToken);
                    }
                    auctionDbContext.Entry(lot).State = EntityState.Deleted;
                }

                auctionDbContext.SaveChanges();
                transaction.Commit();
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                throw new Exception(ex.Message);
            }
        }
    }

    public Task DeleteSpoiledLots(CancellationToken cancellationToken)
    {
        using (var transaction = auctionDbContext.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        {
            try
            {
                var lotsToClose = auctionDbContext.AuctionLots
                    .Where(x => x.LotSpoiledAt < DateTime.UtcNow)
                    .ToList();

                foreach (var lot in lotsToClose)
                {
                    auctionDbContext.Entry(lot).State = EntityState.Deleted;
                }

                auctionDbContext.SaveChanges();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception(ex.Message);
            }
        }

        return Task.CompletedTask;
    }
    public async Task<PagingStoreResponse<AuctionLotRecord>> GetPagingLots(PagingStoreRequest contract, CancellationToken cancellationToken)
    {
        var lots = await auctionDbContext.AuctionLots
            .Where(x => x.LotStatus == AuctionLotStatusRecord.Active)
            .OrderBy(x => x.LotExpiredAt)
            .ThenBy(x => x.LotSpoiledAt)
            .Skip((contract.PageNumber - 1) * contract.PageSize)
            .Take(contract.PageSize)
            .ToListAsync(cancellationToken);

        int pageCount = lots.Count % contract.PageSize == 0 ? 
            lots.Count / contract.PageSize :
            lots.Count / contract.PageSize + 1;

        int totalCount = await auctionDbContext.AuctionLots.CountAsync(cancellationToken);
        return new PagingStoreResponse<AuctionLotRecord>
            (
                pageCount: pageCount,
                totalCount: totalCount,
                pageItems: lots
            );
    }

    public async Task<AuctionLotRecord> TryBuyLot(AuctionLotStoreBuyRequest inputContract, CancellationToken cancellationToken)
    {
        using (var transaction = auctionDbContext.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        {
            try
            {
                var userRecord = await auctionDbContext.Users
                    .FirstAsync(x => x.Id == inputContract.UserId, cancellationToken)
                    ?? throw new InvalidOperationException("Такого пользователя не найдено!");

                var auctionLotRecord = await auctionDbContext.AuctionLots
                    .FirstOrDefaultAsync(x => x.Id == inputContract.AuctionLotId, cancellationToken)
                    ?? throw new InvalidOperationException("Такого лота не найдено!");

                if (auctionLotRecord.LotStatus != AuctionLotStatusRecord.Active)
                {
                    throw new InvalidOperationException("Этот лот не активен!");
                }

                var notificationMessage = new NotificationMessageRecord()
                {
                    Id = Guid.NewGuid(),
                    EmailAdress = userRecord.Email,
                    Message = $"Чек по лоту {auctionLotRecord.Id}"
                };
                await auctionDbContext.NotificationMessages.AddAsync(notificationMessage, cancellationToken);
                

                auctionLotRecord.WinningBidderId = userRecord.Id;
                auctionLotRecord.CurrentBidderId = userRecord.Id;
                auctionLotRecord.CurrentBid = auctionLotRecord.WinningBid;
                auctionLotRecord.LotStatus = AuctionLotStatusRecord.Sold;
                auctionDbContext.Entry(auctionLotRecord).State = EntityState.Modified;

                auctionDbContext.SaveChanges();
                transaction.Commit();
                return auctionLotRecord;
            }
            catch (InvalidOperationException ex) 
                when (ex.Message == "Такого пользователя не найдено!" 
                || ex.Message == "Такого лота не найдено!"
                || ex.Message == "Этот лот не активен!")
            {
                transaction.Rollback();
                throw new InvalidOperationException($"{ex.Message}");
            }
            catch (InvalidOperationException ex)
                when (ex.Message == "An exception has been raised that is likely due to a transient failure.")
            {
                transaction.Rollback();
                throw new InvalidOperationException($"Кто-то уже выкупает лот!");
            }
            catch(DbUpdateException ex)
            {
                transaction.Rollback();
                throw new InvalidOperationException("Кто-то уже выкупает лот!");
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception(ex.Message);
            }
        }
    }

    public async Task<AuctionLotRecord> TryMakeBid(AuctionLotStoreMakeBidRequest inputContract, CancellationToken cancellationToken)
    {
        using (var transaction = auctionDbContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
        {
            try
            {
                var userRecord = await auctionDbContext.Users
                    .FirstAsync(x => x.Id == inputContract.UserId, cancellationToken)
                    ?? throw new InvalidOperationException("Такого пользователя не найдено!");

                var auctionLotRecord = await auctionDbContext.AuctionLots
                    .FirstOrDefaultAsync(x => x.Id == inputContract.AuctionLotId, cancellationToken)
                    ?? throw new InvalidOperationException("Такого лота не найдено!");

                if (auctionLotRecord.LotStatus != AuctionLotStatusRecord.Active)
                {
                    throw new InvalidOperationException("Этот лот не активен!");
                }

                if(inputContract.AuctionBid > auctionLotRecord.CurrentBid)
                {
                    if(auctionLotRecord.CurrentBidderId != null)
                    {
                        var notificationMessage = new NotificationMessageRecord()
                        {
                            Id = Guid.NewGuid(),
                            EmailAdress = (await auctionDbContext.Users.FirstOrDefaultAsync(x => x.Id == auctionLotRecord.CurrentBidderId, cancellationToken)).Email,
                            Message = $"Вас опередили по лоту {auctionLotRecord.Id}"
                        };

                        await auctionDbContext.NotificationMessages.AddAsync(notificationMessage, cancellationToken);
                    }
                    
                    auctionLotRecord.CurrentBidderId = userRecord.Id;
                    auctionLotRecord.CurrentBid = inputContract.AuctionBid;
                    auctionDbContext.Entry(auctionLotRecord).State = EntityState.Modified;

                    auctionDbContext.SaveChanges();
                    transaction.Commit();
                    return auctionLotRecord;
                }
                else 
                {
                    throw new InvalidOperationException("Ставка должна быть больше текущей ставки!");
                }
                
            }
            catch(InvalidOperationException ex)
            {
                transaction.Rollback();
                throw new InvalidOperationException(ex.Message);
            }
            catch (DbUpdateException ex)
            {
                transaction.Rollback();
                throw new Exception("Кто-то уже пытается выкупить лот!");
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception(ex.Message);
            }
        }
    }

    public async Task<IReadOnlyCollection<NotificationMessageRecord>> GetNotificationMessageRecords(CancellationToken cancellationToken)
    {
        var messages = await auctionDbContext.NotificationMessages
            .ToListAsync();

        return messages;
    }

    public async Task DeleteNotificationMessages(IReadOnlyCollection<Guid> messagesIds, CancellationToken cancellationToken)
    {
        await auctionDbContext.NotificationMessages
            .Where(x => messagesIds.Contains(x.Id))
            .ExecuteDeleteAsync();


        await auctionDbContext.SaveChangesAsync();
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}