﻿namespace Store.Repositories.Abstract.StoreContracts.Input;

public class RegisterUserStoreRequest
{
    public string Email { get; set; }

    public string Password { get; set; }

    public RegisterUserStoreRequest(string email, string password)
    {
        Email = email;
        Password = password;
    }
}
