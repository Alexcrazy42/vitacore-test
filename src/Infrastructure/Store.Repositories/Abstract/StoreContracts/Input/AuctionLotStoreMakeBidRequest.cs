﻿namespace Store.Repositories.Abstract.StoreContracts.Input;

public class AuctionLotStoreMakeBidRequest
{
    public Guid UserId { get; set; }

    public Guid AuctionLotId { get; set; }

    public decimal AuctionBid { get; set; }
}