﻿namespace Store.Repositories.Abstract.StoreContracts.Input;

public class PagingStoreRequest
{
    public int PageNumber { get; set; }

    public int PageSize { get; set; }

    public PagingStoreRequest(int pageNumber, int pageSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
    }
}
