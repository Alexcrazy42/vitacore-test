﻿namespace Store.Repositories.Abstract.StoreContracts.Input;

public class AuctionLotStoreBuyRequest
{
    public Guid UserId { get; set; }

    public Guid AuctionLotId { get; set; }

    public AuctionLotStoreBuyRequest(Guid userId, Guid auctionLotId)
    {
        UserId = userId;
        AuctionLotId = auctionLotId;
    }
}