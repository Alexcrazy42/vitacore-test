﻿namespace Store.Repositories.Abstract.StoreContracts.Output;

public class PagingStoreResponse<T>
{
    public int PageCount { get; set; }

    public int TotalCount { get; set; }

    public IReadOnlyCollection<T> PageItems { get; set; } = new List<T>();

    public PagingStoreResponse(int pageCount, int totalCount, IReadOnlyCollection<T> pageItems)
    {
        PageCount = pageCount;
        TotalCount = totalCount;
        PageItems = pageItems;
    }
}
