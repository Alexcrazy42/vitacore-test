﻿using Store.Db.DbRecords;
using Store.Repositories.Abstract.StoreContracts.Input;

namespace Store.Repositories.Abstract;

public interface IUserRepository
{
    public Task<UserRecord> GetUserByEmail(string email, CancellationToken cancellationToken);

    public Task<UserRecord> RegisterUser(RegisterUserStoreRequest request, CancellationToken cancellationToken);
}