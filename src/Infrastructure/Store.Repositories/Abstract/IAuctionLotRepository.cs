﻿using Store.Db.DbRecords;
using Store.Repositories.Abstract.StoreContracts.Input;
using Store.Repositories.Abstract.StoreContracts.Output;

namespace Store.Repositories.Abstract;

public interface IAuctionLotRepository : IDisposable
{
    public Task CreateRandomMandarinLot(CancellationToken cancellationToken);

    public Task<AuctionLotRecord> GetAuctionLotRecord(Guid id, CancellationToken cancellationToken);

    public Task<PagingStoreResponse<AuctionLotRecord>> GetPagingLots(PagingStoreRequest contract, CancellationToken cancellationToken);

    public Task<AuctionLotRecord> TryBuyLot(AuctionLotStoreBuyRequest inputContract, CancellationToken cancellationToken);

    public Task<AuctionLotRecord> TryMakeBid(AuctionLotStoreMakeBidRequest inputContract, CancellationToken cancellationToken);

    public Task CloseAuctionLot(CancellationToken cancellationToken);

    public Task DeleteSpoiledLots(CancellationToken cancellationToken);

    public Task<IReadOnlyCollection<NotificationMessageRecord>> GetNotificationMessageRecords(CancellationToken cancellationToken);

    public Task DeleteNotificationMessages(IReadOnlyCollection<Guid> messagesIds, CancellationToken cancellationToken);
}