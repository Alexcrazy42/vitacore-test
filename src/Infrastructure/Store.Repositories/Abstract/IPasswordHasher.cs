﻿namespace Store.Repositories.Abstract;

public interface IPasswordHasher
{
    internal string GenerateHashPassword(string password);

    public bool VerifyPassword(string password, string hashedPassword);
}
