﻿using Microsoft.EntityFrameworkCore;
using Store.Db.DbRecords;

namespace Store.Db;

public class AuctionDbContext : DbContext
{
    public DbSet<UserRecord> Users => Set<UserRecord>();

    public DbSet<AuctionLotRecord> AuctionLots => Set<AuctionLotRecord>();

    public DbSet<NotificationMessageRecord> NotificationMessages => Set<NotificationMessageRecord>();

    public AuctionDbContext() { }

    public AuctionDbContext(DbContextOptions<AuctionDbContext> contextOptions)
        : base(contextOptions)
    { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        base.OnModelCreating(builder);
    }
}