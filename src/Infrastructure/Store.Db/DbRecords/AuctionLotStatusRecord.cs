﻿namespace Store.Db.DbRecords;

public enum AuctionLotStatusRecord
{
    Sold = 1,
    Expired = 2,
    Active = 4
}