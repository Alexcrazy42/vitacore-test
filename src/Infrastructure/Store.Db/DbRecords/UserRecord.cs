﻿namespace Store.Db.DbRecords;

public class UserRecord
{
    public Guid Id { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }

    public IReadOnlyCollection<AuctionLotRecord> CurrentUserLots { get; set; } = new List<AuctionLotRecord>();

    public IReadOnlyCollection<AuctionLotRecord> WinningUserLots { get; set; } = new List<AuctionLotRecord>();
}