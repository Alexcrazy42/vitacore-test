﻿namespace Store.Db.DbRecords;

public class NotificationMessageRecord
{
    public Guid Id { get; set; }

    public string EmailAdress { get; set; }

    public string Message { get; set; }
}
