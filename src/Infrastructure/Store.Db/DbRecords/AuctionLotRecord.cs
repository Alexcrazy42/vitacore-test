﻿namespace Store.Db.DbRecords;

public class AuctionLotRecord
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public decimal CurrentBid { get; set; }

    public Guid? CurrentBidderId { get; set; }

    public UserRecord? CurrentBidder { get; set; }

    public decimal WinningBid { get; set; }

    public Guid? WinningBidderId { get; set; }

    public UserRecord? WinningBidder { get; set; }

    public AuctionLotStatusRecord LotStatus { get; set; }

    /// <summary>
    /// Это время, до которого лот еще можно продавать,
    /// после лот уходит последнему ставившему
    /// </summary>
    public DateTime LotExpiredAt { get; set; }

    /// <summary>
    /// Если лот может испортиться (как в случае с мандарином), 
    /// у него есть время до которого, его можно продать
    /// </summary>
    public DateTime? LotSpoiledAt { get; set; }
}