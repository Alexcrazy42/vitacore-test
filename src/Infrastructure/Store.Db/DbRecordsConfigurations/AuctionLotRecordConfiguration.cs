﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Store.Db.DbRecords;

namespace Store.Db.DbRecordsConfigurations;

public class AuctionLotRecordConfiguration : IEntityTypeConfiguration<AuctionLotRecord>
{
    public void Configure(EntityTypeBuilder<AuctionLotRecord> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd();

        builder.Property(x => x.Name)
            .HasMaxLength(ConfigConst.MaxLengthOfLotName)
            .IsRequired();
    }
}