﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Store.Db.DbRecords;

namespace Store.Db.DbRecordsConfigurations;

public class NotificationMessageRecordConfiguration : IEntityTypeConfiguration<NotificationMessageRecord>
{
    public void Configure(EntityTypeBuilder<NotificationMessageRecord> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd();

        builder.Property(x => x.EmailAdress)
            .HasMaxLength(ConfigConst.MaxLengthOfEmail)
            .IsRequired();

        builder.Property(x => x.Message)
            .HasMaxLength(ConfigConst.MaxLengthOfNotificationMessage)
            .IsRequired();
    }
}
