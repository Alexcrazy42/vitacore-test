﻿namespace Store.Db.DbRecordsConfigurations;

public static class ConfigConst
{
    // userRecord
    public const int MaxLengthOfEmail = 256;

    public const int MaxLengthOfPassword = 100;

    // auctionLotRecord
    public const int MaxLengthOfLotName = 100;

    // notificationMessageRecord
    public const int MaxLengthOfNotificationMessage = 300;
}
