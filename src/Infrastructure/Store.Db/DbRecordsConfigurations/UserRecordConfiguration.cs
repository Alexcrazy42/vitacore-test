﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Store.Db.DbRecords;

namespace Store.Db.DbRecordsConfigurations; 

public class UserRecordConfiguration : IEntityTypeConfiguration<UserRecord>
{
    public void Configure(EntityTypeBuilder<UserRecord> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd();

        builder
            .Property(x => x.Email)
            .HasMaxLength(ConfigConst.MaxLengthOfEmail);

        builder
            .Property(x => x.Password)
            .HasMaxLength(ConfigConst.MaxLengthOfPassword);

        builder
            .HasMany(u => u.CurrentUserLots)
            .WithOne(l => l.CurrentBidder)
            .HasForeignKey(l => l.CurrentBidderId)
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasMany(u => u.WinningUserLots)
            .WithOne(l => l.WinningBidder)
            .HasForeignKey(l => l.WinningBidderId)
            .OnDelete(DeleteBehavior.NoAction);
    }
}
