﻿namespace Store.Db.Configurations;

public enum DbProvider
{
    PostgreSQL = 1,
    SQLite = 2, 
    Redis = 3
}