﻿namespace Store.Db.Configurations;

public class DatabaseOptions
{
    public DbProvider Provider { get; set; }

    public string? ConnectionString { get; set; }
}
