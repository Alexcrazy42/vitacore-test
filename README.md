# Как бы описал README для проекта

## О проекте
AuctionService - сервис аукционов, где пользователи могут: делать ставки, выкупать лоты. Есть hosted сервисы, отвечающие за: рассылку уведомлений, удаление испорченных и выкупленных лотов

## Документация
....

## Проекты
#### Web
Стартует HTTP-сервер

Запуск с помощью Docker
```shell
docker build -t auction-service .
docker run -p 8080:80 auction-service
```

#### Миграции
Создание миграции
```shell
cd src/Infrastructure/Store.Db.Migrations
dotnet ef migrations add NameOfNewMigration
```

#### Структура решения
```
├───src
│   ├───Core <- слой бизнес логики
│   │   ├───Domain
│   │   │   ├───Models <- модели слоя бизнес логики
│   │   └───Services <- сервисы, описывающие use-cases
│   │       ├───Abstract
│   │       ├───Configurations
│   │       ├───Implementations
│   ├───Infrastructure <- слой persistence данных
│   │   ├───Store.Db <- проект для описания БД
│   │   │   ├───DbRecords <- модели БД
│   │   │   ├───DbRecordsConfigurations <- fluent описание моделей БД
│   │   ├───Store.Db.Migrations <- проект миграций
│   │   │   ├───Migrations <- миграции
│   │   └───Store.Repositories <- проект репозиториев для доступа к persistence данным
│   └───Web <- слой представления
│       ├───Web
│       │   ├───ApiVersioning
│       │   ├───Controllers
│       │   ├───Infrastructure
│       └───Web.Contracts
│           ├───Dtos <- дтошки для проекта Web
|           ├───Infrastructure <- надстройка над ServiceCollection и добавление FluentValidation
└───tests <- тесты
```

# Добавляю от чисто от себя
Задания все выполнены

## Библиотеки в C# решении:
- Automapper
- DI
- EntityFramework (Npgsql)
- Swagger

## Решил не тянуть дольше недели с тестовым заданием, поэтому не довел до ума, но хочу сделать:
- паттерн UnitOfWork, потому что бизнес логика начала "протекать" в транзакции
- Unit и Integration тесты, особенно на тест кейсы борьбы за ресурсы от разных пользователей
- Central Package Management
- RetryPolicy

## Удалось поработать с новым:
- HostedServices
- Уровни изоляции транзакций
- Аутентификация
- Хеширование данных в БД

## Чтобы я сделал для архитектуры:
- Создание микросервиса NotificationService, с которым бы IPC было налажено через брокер сообщений
- Создание микросервиса JobService, который выполнял бы работы по расписанию
- Добавление Distributed Cache в виде Redis (например, чтобы хранить там самые популярные лоты на сегодня, изначально так и хотел сделать)
- Вынесение всей чувствительной информации из appsettings.json в какое-нибудь хранилище секретов

## На изучение каких тем навело выполнение тестового задания:
- Транзакции и уровни изоляции
- Аутентификация и авторизация: refresh token, OAuth, OIDC, внешние провайдеры

# Спасибо за интересное тестовое задание!
