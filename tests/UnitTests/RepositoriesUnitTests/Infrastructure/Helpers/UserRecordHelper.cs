﻿using Store.Db.DbRecords;

namespace RepositoriesUnitTests.Infrastructure.Helpers;

public static class UserRecordHelper
{
    public static UserRecord GetOne(string id = "14dc1760-1ba6-4ae5-acdd-7c92455f7bb1")
    {
        return new UserRecord()
        {
            Id = Guid.Parse(id),
            Email = "email1",
            Password = "password1",
        };
    }

    public static IEnumerable<UserRecord> GetMany()
    {
        yield return GetOne();
        yield return new UserRecord()
        {
            Id = Guid.Parse("14dc1760-1ba6-4ae5-acdd-7c92455f7bb2"),
            Email = "email2",
            Password = "password2",
        };
    }
}
