﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Store.Db;

namespace RepositoriesUnitTests.Infrastructure.Helpers;

public class DbContextHelper
{
    public AuctionDbContext DbContext { get; set; }

    public DbContextHelper()
    {
        var builder = new DbContextOptionsBuilder<AuctionDbContext>();
        builder.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
            .UseInMemoryDatabase("Unit tests");
        
        var options = builder.Options;
        DbContext = new AuctionDbContext(options);
    }

    public void AddTestData()
    {
        DbContext.AddRange(AuctionLotRecordHelper.GetMany());
        DbContext.AddRange(UserRecordHelper.GetMany());
        DbContext.SaveChanges();
    }
}
