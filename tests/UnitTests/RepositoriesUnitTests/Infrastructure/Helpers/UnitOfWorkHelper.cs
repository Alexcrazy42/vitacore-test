﻿using Calabonga.UnitOfWork;
using Moq;
using Store.Db;
using Store.Db.DbRecords;

namespace RepositoriesUnitTests.Infrastructure.Helpers;

public class UnitOfWorkHelper
{
    public static Mock<IUnitOfWork<AuctionDbContext>> GetMock()
    {
        var dbContextHelper = new DbContextHelper();
        var context = dbContextHelper.DbContext;
        dbContextHelper.AddTestData();


        
        // repositories
        var unitOfWork = new Mock<IUnitOfWork<AuctionDbContext>>();
        unitOfWork.Setup(x => x.GetRepository<AuctionLotRecord>(false)).Returns(new Repository<AuctionLotRecord>(context));

        var scope1 = context.Database.BeginTransaction();
        var scope2 = context.Database.BeginTransactionAsync();
        unitOfWork.Setup(x => x.BeginTransaction(false)).Returns(scope1);
        unitOfWork.Setup(x => x.BeginTransactionAsync(false)).Returns(scope2);

        return unitOfWork;
    }
}
