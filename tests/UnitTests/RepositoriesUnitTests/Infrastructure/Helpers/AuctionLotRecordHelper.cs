﻿using Store.Db.DbRecords;

namespace RepositoriesUnitTests.Infrastructure.Helpers;

public static class AuctionLotRecordHelper
{
    public static AuctionLotRecord GetOne(string id = "14dc1760-1ba6-4ae5-acdd-7c92455f7bb1")
    {
        return new AuctionLotRecord()
        {
            Id = Guid.Parse(id),
            Name = "lot1",
            CurrentBid = 0,
            WinningBid = 100
        };
    }

    public static IEnumerable<AuctionLotRecord> GetMany()
    {
        yield return GetOne();
        yield return new AuctionLotRecord()
        {
            Id = Guid.Parse("14dc1760-1ba6-4ae5-acdd-7c92455f7bb2"),
            Name = "lot2",
            CurrentBid = 0,
            WinningBid = 100
        };
    }
}
