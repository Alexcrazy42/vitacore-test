﻿using Calabonga.UnitOfWork;
using RepositoriesUnitTests.Infrastructure.Helpers;
using Store.Db;

namespace RepositoriesUnitTests.Infrastructure.Fixture;

public class UnitOfWorkFixture
{
    public IUnitOfWork<AuctionDbContext> Create()
    {
        var mock = UnitOfWorkHelper.GetMock();
        return mock.Object;
    }
}