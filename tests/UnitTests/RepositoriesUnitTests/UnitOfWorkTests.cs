﻿using RepositoriesUnitTests.Infrastructure.Fixture;
using Store.Db.DbRecords;
using FluentAssertions;
using Calabonga.UnitOfWork;

namespace RepositoriesUnitTests;

public class UnitOfWorkTests : IClassFixture<UnitOfWorkFixture>
{
    private readonly IUnitOfWork sut;

    public UnitOfWorkTests(UnitOfWorkFixture fixture)
    {
        sut = fixture.Create();
    }

    [Fact]
    [Trait(nameof(UnitOfWorkTests), "UnderTesting")]
    public void ItShould_UnitOfWork_InstanceCreated()
    {
        // arrange

        // act 

        // assert
        sut.Should().NotBeNull();
    }

    [Fact]
    [Trait(nameof(UnitOfWorkTests), "UnderTesting")]
    public void ItShould_Contains_TwoAuctionLot()
    {
        // arrange
        const int expected = 2;

        // act 
        var actual = sut.GetRepository<AuctionLotRecord>()
            .Count();

        // assert
        actual.Should().Be(expected);
    }

    [Fact]
    [Trait(nameof(UnitOfWorkTests), "UnderTesting")]
    public void ItShould_Contains_TwoUsers()
    {
        // arrange
        const int expected = 2;

        // act 
        var actual = sut.GetRepository<UserRecord>()
            .Count();

        // assert
        actual.Should().Be(expected);
    }
}
