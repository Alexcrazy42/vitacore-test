﻿using AutoFixture;
using Calabonga.UnitOfWork;
using Moq;
using RepositoriesUnitTests.Infrastructure.Fixture;
using Store.Db.DbRecords;
using Store.Repositories.Abstract;

namespace RepositoriesUnitTests;

public class UserRepositoryUnitTests
{
    private readonly IUnitOfWork sut;
    private readonly Mock<IPasswordHasher> passwordHasherMock;


    public UserRepositoryUnitTests(UnitOfWorkFixture fixture)
    {
        sut = fixture.Create();
        passwordHasherMock = new Mock<IPasswordHasher>();

    }

    [Theory]
    [InlineData("email3")]
    [InlineData("email4")]
    public void GetUserByEmail_WhenNonExistentEmail_ThrowException(string email)
    {
        // arrange
        
        // act
        

        // assert
    }
}
